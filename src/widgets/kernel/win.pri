# Qt/Windows only configuration file
# --------------------------------------------------------------------

INCLUDEPATH += ../3rdparty/wintab
!winrt: LIBS_PRIVATE *= -lshell32
# Override MinGW's definition in _mingw.h
mingw: DEFINES += WINVER=0x0501 _WIN32_WINNT=0x0501
